// ==UserScript==
// @name        GGn Description Preview
// @namespace   green-is-my-paddy
// @version     2.1.1
// @description BBCode preview for description fields on GGn.
// @author      mujau
// @homepageURL https://gitlab.com/mujau/ggn-description-preview
// @supportURL  https://gitlab.com/mujau/ggn-description-preview/-/issues
// @downloadURL https://gitlab.com/mujau/ggn-description-preview/-/raw/master/dist/ggn-description-preview.user.js
// @require     https://unpkg.com/@preact/signals-core@1.8.0/dist/signals-core.min.js
// @grant       GM_addStyle
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addValueChangeListener
// @grant       GM_registerMenuCommand
// @grant       GM_unregisterMenuCommand
// @run-at      document-end
// @match       https://gazellegames.net/upload.php
// @match       https://gazellegames.net/torrents.php*
// @match       https://gazellegames.net/collections.php*
// @match       https://gazellegames.net/requests.php*
// @match       https://gazellegames.net/inbox.php*
// @match       https://gazellegames.net/staffpm.php*
// @match       https://gazellegames.net/forums.php*
// @match       https://gazellegames.net/reports.php*
// @match       https://gazellegames.net/reportsv2.php*
// @match       https://gazellegames.net/wiki.php*
// @match       https://gazellegames.net/user.php*
// ==/UserScript==

"use strict";

// src/getPageName.ts
function isUploadPage({ pathname }) {
  return pathname === "/upload.php";
}
function isReleaseEditPage({ pathname, searchParams }) {
  return pathname === "/torrents.php" && searchParams.has("action", "edit");
}
function isGroupEditPage({ pathname, searchParams }) {
  return pathname === "/torrents.php" && searchParams.has("action", "editgroup");
}
function isGroupPage({ pathname, searchParams }) {
  return pathname === "/torrents.php" && searchParams.has("id");
}
function isAddTorrentLinkPage({ pathname, searchParams }) {
  return pathname === "/torrents.php" && searchParams.has("action", "add_torrent_link");
}
function isCollectionPage({ pathname, searchParams }) {
  return pathname === "/collections.php" && searchParams.has("id");
}
function isCollectionNewOrEditPage({ pathname, searchParams }) {
  return pathname === "/collections.php" && (searchParams.has("action", "new") || searchParams.has("action", "edit"));
}
function isNewRequestPage({ pathname, searchParams }) {
  return pathname === "/requests.php" && searchParams.has("action", "new");
}
function isViewRequestPage({ pathname, searchParams }) {
  return pathname === "/requests.php" && searchParams.has("action", "view");
}
function isPmPage({ pathname, searchParams }) {
  return pathname === "/inbox.php" && (searchParams.has("action", "compose") || searchParams.has("action", "viewconv"));
}
function isStaffPmPage({ pathname }) {
  return pathname === "/staffpm.php";
}
function isViewThreadPage({ pathname, searchParams }) {
  return pathname === "/forums.php" && searchParams.has("action", "viewthread");
}
function isNewThreadPage({ pathname, searchParams }) {
  return pathname === "/forums.php" && searchParams.has("action", "new");
}
function isReportV1Page({ pathname, searchParams }) {
  return pathname === "/reports.php" && searchParams.has("action", "report");
}
function isReportV2Page({ pathname, searchParams }) {
  return pathname === "/reportsv2.php" && searchParams.has("action", "report");
}
function isWikiCreateOrEditPage({ pathname, searchParams }) {
  return pathname === "/wiki.php" && (searchParams.has("action", "create") || searchParams.has("action", "edit"));
}
function isUserEditPage({ pathname, searchParams }) {
  return pathname === "/user.php" && searchParams.has("action", "edit");
}
var PAGE_LABELS = [
  "UPLOAD",
  "RELEASE_EDIT",
  "GROUP_EDIT",
  "GROUP",
  "ADD_TORRENT_LINK",
  "COLLECTION",
  "COLLECTION_NEW_OR_EDIT",
  "NEW_REQUEST",
  "VIEW_REQUEST",
  "PM",
  "STAFF_PM",
  "VIEW_THREAD",
  "NEW_THREAD",
  "REPORT_V1",
  "REPORT_V2",
  "WIKI_CREATE_OR_EDIT",
  "USER_EDIT"
];
function lowercase_pagify(names) {
  const e = /* @__PURE__ */ Object.create(null);
  for (const name of names) {
    e[name] = `${name.toLowerCase()}_page`;
  }
  return Object.freeze(e);
}
var PAGE = lowercase_pagify(PAGE_LABELS);
var matchers = [
  [isUploadPage, PAGE.UPLOAD],
  [isReleaseEditPage, PAGE.RELEASE_EDIT],
  [isGroupEditPage, PAGE.GROUP_EDIT],
  [isGroupPage, PAGE.GROUP],
  [isAddTorrentLinkPage, PAGE.ADD_TORRENT_LINK],
  [isCollectionPage, PAGE.COLLECTION],
  [isCollectionNewOrEditPage, PAGE.COLLECTION_NEW_OR_EDIT],
  [isNewRequestPage, PAGE.NEW_REQUEST],
  [isViewRequestPage, PAGE.VIEW_REQUEST],
  [isPmPage, PAGE.PM],
  [isStaffPmPage, PAGE.STAFF_PM],
  [isViewThreadPage, PAGE.VIEW_THREAD],
  [isNewThreadPage, PAGE.NEW_THREAD],
  [isReportV1Page, PAGE.REPORT_V1],
  [isReportV2Page, PAGE.REPORT_V2],
  [isWikiCreateOrEditPage, PAGE.WIKI_CREATE_OR_EDIT],
  [isUserEditPage, PAGE.USER_EDIT]
];
function getPageName(url) {
  for (const [matcher, name] of matchers) {
    if (!matcher(url)) continue;
    console.debug(`BBCode preview: ${name} matched`);
    return name;
  }
}

// src/utils/getElements.ts
var NotFoundError = class extends Error {
  constructor(message, cause) {
    super(message, { cause });
    this.name = "NotFoundError";
  }
};
var isArray = Array.isArray;
function getElements(selectors, throwOnMissing = true) {
  const elems = {};
  for (const [key, val] of Object.entries(selectors)) {
    const [root, selector, isOptional] = isArray(val) ? val : [document, val, false];
    const rootNode = typeof root !== "string" ? root : elems[root];
    if (!(rootNode instanceof Node)) {
      if (rootNode === void 0) {
        throw new ReferenceError(`Root element \`${root}\` isn't defined`);
      } else if (rootNode === null) {
        if (!throwOnMissing) {
          elems[key] = null;
          continue;
        } else {
          throw new ReferenceError(`Root element \`${root}\` is null`);
        }
      } else {
        throw new Error("Something happened.");
      }
    }
    const maybeElem = rootNode.querySelector(selector);
    if (!maybeElem && throwOnMissing && !isOptional) {
      throw new NotFoundError(`\`${key}\` not found`, key);
    }
    elems[key] = maybeElem;
  }
  return elems;
}

// src/common.ts
function errorHandler(err, scope, graceful = false) {
  if (err instanceof NotFoundError) {
    const msg = `BBCode preview: ${scope} element \`${err.cause}\` not found`;
    if (!graceful) {
      throw new Error(msg);
    } else {
      console.debug(msg);
    }
  } else {
    throw err;
  }
}

// src/BBCode.ts
function spoilerShim(elem) {
  elem.removeAttribute("onclick");
  elem.addEventListener("click", () => {
    const nextSibling = elem.nextElementSibling;
    if (!nextSibling) return;
    elem.innerText = nextSibling.classList.contains("hidden") ? "Hide" : "Show";
    nextSibling.classList.toggle("hidden");
  });
}
function redactedShim(elem) {
  elem.addEventListener(
    "mouseenter",
    () => elem.classList.replace("redacted_bbcode_hidden", "redacted_bbcode_show")
  );
  elem.addEventListener(
    "mouseout",
    () => elem.classList.replace("redacted_bbcode_show", "redacted_bbcode_hidden")
  );
}
function shim(elem) {
  elem.querySelectorAll("[onclick='BBCode.spoiler(this);']").forEach((e) => spoilerShim(e));
  elem.querySelectorAll(".redacted_bbcode").forEach((e) => redactedShim(e));
}
var BBCode = unsafeWindow.bbcodeOnPageLoad ? () => {
  console.debug("BBCode preview: applying BBCode magic...");
  unsafeWindow.bbcodeOnPageLoad();
} : (root) => {
  console.debug("BBCode preview: `bbcode.js` unavailable. Shimming...");
  shim(root);
};

// src/external.ts
var { effect, signal } = preactSignalsCore;

// src/livePreviewState.ts
var KEY = "LIVE_PREVIEW_STATE";
var id = null;
var livePreview = signal(GM_getValue(KEY, false));
function register() {
  if (id) GM_unregisterMenuCommand(id);
  id = GM_registerMenuCommand(
    `${livePreview.value ? "Disable" : "Enable"} live preview`,
    () => {
      livePreview.value = !livePreview.value;
      GM_setValue(KEY, livePreview.value);
      register();
    }
  );
}
register();
GM_addValueChangeListener(KEY, (key, oldVal, val) => {
  livePreview.value = val;
  register();
});

// src/utils/debounce.ts
function debounce(fn, delay, { onTick, tickInterval, onSetup } = {}) {
  let timeout;
  let interval;
  function debounced(...args) {
    onSetup?.();
    if (timeout) clearTimeout(timeout);
    if (interval) clearInterval(interval);
    timeout = setTimeout(() => {
      fn.apply(this, args);
      if (interval) clearInterval(interval);
    }, delay);
    if (onTick) interval = setInterval(onTick, tickInterval);
  }
  function dispose() {
    if (timeout) clearTimeout(timeout);
    if (interval) clearInterval(interval);
  }
  return [debounced, dispose];
}

// src/utils/html.ts
function html(strings, ...subs) {
  const template = document.createElement("template");
  const arr = [strings[0]];
  subs.forEach(
    (val, i) => arr.push(
      `${typeof val === "string" ? val : `<param i=${i}>`}`,
      strings[i + 1]
    )
  );
  template.innerHTML = arr.join("");
  template.content.querySelectorAll("param[i]").forEach((e) => {
    const sub = subs[+e.getAttribute("i")];
    sub instanceof Array ? e.replaceWith(...sub) : e.replaceWith(sub);
  });
  return template.content.children.length > 1 ? template.content : template.content.children[0];
}
function collectRefs(root) {
  const refs = {};
  root.querySelectorAll("[ref]").forEach((e) => {
    refs[e.getAttribute("ref")] = e;
    e.removeAttribute("ref");
  });
  return refs;
}

// src/utils/propertyObserver.ts
function propertyObserver(obj, prop, callback) {
  const prototype = Object.getPrototypeOf(obj);
  const descriptor = Object.getOwnPropertyDescriptor(prototype, prop);
  if (!descriptor) {
    throw new ReferenceError(
      `Input object doesn't have ${String(prop)} property`
    );
  }
  const set = descriptor.set;
  if (!set) {
    throw new ReferenceError(`${String(prop)} property doesn't have a setter`);
  }
  descriptor.set = (value) => {
    const oldValue = obj[prop];
    callback(value, oldValue);
    set.call(obj, value);
  };
  Object.defineProperty(obj, prop, descriptor);
}

// src/preview.ts
function getQuickReplyPreview() {
  try {
    const { root, inner } = getElements({
      root: "#quickreplypreview",
      inner: ["root", "#contentpreview"]
    });
    root.classList.remove("hidden");
    return { root, inner };
  } catch (err) {
    throw errorHandler(err, "quick reply");
  }
}
function getNewThreadPreview() {
  try {
    const { root, inner, ...other } = getElements({
      root: "#newthreadpreview",
      inner: "#contentpreview",
      threadTitle: "#newthreadtitle",
      form: "form#newthreadform",
      pollQuestion: "#poll_question",
      pollAnswers: "#poll_answers",
      threadPoll: "#threadpoll"
    });
    root.classList.remove("hidden");
    return { root, inner, other };
  } catch (err) {
    throw errorHandler(err, "new thread");
  }
}
function getGroupTemplate() {
  const root = html`<div class="box">
    <div class="body" ref="inner"></div>
  </div>`;
  const { inner } = collectRefs(root);
  return { root, inner };
}
function getReleaseTemplate() {
  const root = html`<table class="torrent_table">
    <tbody>
      <tr>
        <td>
          <blockquote class="description_blockquote" ref="inner"></blockquote>
        </td>
      </tr>
    </tbody>
  </table>`;
  const { inner } = collectRefs(root);
  return { root, inner };
}
function getControls() {
  const root = html`<div class="無-bbcode__controls">
    <input
      type="button"
      class="無-bbcode__preview-button"
      value="Preview"
      ref="previewButton"
    />
  </div>`;
  const { previewButton } = collectRefs(root);
  return { root, previewButton };
}
var previewBoxMap = {
  group: getGroupTemplate,
  release: getReleaseTemplate,
  post: getQuickReplyPreview,
  thread: getNewThreadPreview
};
function getPreview(type) {
  const box = previewBoxMap[type]();
  const root = html`<div class="無-bbcode__preview-container hidden">
    ${box.root}
  </div>`;
  const other = "other" in box ? box.other : null;
  return { root, inner: box.inner, other };
}
var DEBOUNCE_DELAY = 2e3;
var Preview = class {
  controls = getControls();
  preview;
  #isHidden = false;
  #isPreviewOpened = false;
  #isPreviewAllowed = signal(false);
  #previewType;
  #source = "";
  #markup = "";
  #countdown = signal(Infinity);
  #effects;
  constructor({
    target,
    where = "beforeend",
    props: { textarea, type = "group" }
  }) {
    this.preview = getPreview(type);
    this.#previewType = type;
    if (target) {
      target.insertAdjacentElement(where, this.controls.root);
      this.controls.root.after(this.preview.root);
    }
    this.controls.previewButton.addEventListener("click", this.#preview);
    this.#isPreviewAllowed.value = !!textarea.value;
    this.#source = textarea.value;
    this.#effects = [
      /* Preview button text */
      effect(() => {
        this.controls.previewButton.value = livePreview.value ? this.#countdown.value === Infinity ? "Live preview enabled" : `Live preview in ${this.#countdown.value}...` : "Preview";
      }),
      /* Preview button disabled state */
      effect(() => {
        this.controls.previewButton.disabled = livePreview.value || !this.#isPreviewAllowed.value;
      })
    ];
    const [debouncedPreview] = debounce(
      () => {
        this.#preview();
        this.#countdown.value = Infinity;
      },
      DEBOUNCE_DELAY,
      {
        onTick: () => this.#countdown.value--,
        tickInterval: 1001,
        onSetup: () => this.#countdown.value = DEBOUNCE_DELAY / 1e3
      }
    );
    const textareaValueChange = (val) => {
      this.#source = val;
      if (livePreview.value) {
        console.log("BBCode preview: debouncing live preview...");
        debouncedPreview();
      } else {
        this.#isPreviewAllowed.value = !!val;
      }
    };
    textarea.addEventListener("input", () => {
      textareaValueChange(textarea.value);
    });
    propertyObserver(textarea, "value", (val) => {
      textareaValueChange(val);
    });
    if (type === "thread") {
      const { threadTitle, form, pollQuestion, pollAnswers } = this.preview.other;
      const titleInput = form.elements["title"];
      const [debouncedPollPreview] = debounce(this.#pollPreview, 500);
      titleInput.addEventListener(
        "input",
        () => threadTitle.innerText = titleInput.value
      );
      pollQuestion.addEventListener("input", debouncedPollPreview);
      pollAnswers.addEventListener("input", debouncedPollPreview);
    }
  }
  #togglePreviewOpened(open) {
    this.#isPreviewOpened = open ?? !this.#isPreviewOpened;
    this.preview.root.classList.toggle("hidden", !this.#isPreviewOpened);
  }
  #renderPreview() {
    this.preview.inner.innerHTML = this.#markup;
    BBCode(this.preview.inner);
  }
  #pollPreview = () => {
    const { form, threadPoll } = this.preview.other;
    const elements = form.elements;
    const answers = Symbol.iterator in elements["answers[]"] ? elements["answers[]"] : [elements["answers[]"]];
    const elems = [];
    let i = 1;
    for (const { value } of answers) {
      const s = i.toString();
      elems.push(
        html`<li>
          <input type="radio" name="vote" id="answer_${s}" value="${s}" />
          <label for="answer_${s}">${value}</label>
          <br />
        </li>`
      );
      i++;
    }
    threadPoll.replaceChildren(
      html`<p><strong>${elements["question"].value}</strong></p>
        <div id="poll_results">
          <ul style="list-style: none;" id="poll_options">
            ${elems}
            <li>
              <br />
              <input type="radio" name="vote" id="answer_0" value="0" />
              <label for="answer_0">Blank - Show the results!</label>
              <br />
            </li>
          </ul>
          <input type="button" style="float: left;" value="Vote" />
        </div>`
    );
  };
  #preview = async () => {
    console.debug("BBCode preview: previewing...");
    this.#isPreviewAllowed.value = false;
    if (!this.#source) {
      console.debug("BBCode preview: tried to preview nothing");
      return;
    }
    const formdata = new FormData();
    formdata.append("body", this.#source);
    try {
      const response = await fetch("ajax.php?action=preview", {
        method: "POST",
        headers: {
          "x-requested-with": "XMLHttpRequest"
        },
        body: formdata
      });
      this.#markup = await response.text();
    } catch (err) {
      if (err instanceof Error) this.#markup = err.message;
      this.#isPreviewAllowed.value = true;
      throw err;
    } finally {
      this.#renderPreview();
      this.#togglePreviewOpened(true);
      if (this.#previewType === "thread") this.#pollPreview();
    }
  };
  toggleHidden = (hide) => {
    this.#isHidden = hide ?? !this.#isHidden;
    this.controls.root.classList.toggle("hidden", this.#isHidden);
    if (this.#isHidden || this.#isPreviewOpened) {
      this.preview.root.classList.toggle("hidden", this.#isHidden);
    }
  };
  destroy = () => {
    this.#effects.forEach((dispose) => dispose());
  };
};

// src/previewHandlers.ts
function markParentCell({ parentElement }) {
  if (!(parentElement instanceof HTMLTableCellElement)) return;
  parentElement.classList.add("無-bbcode__desc_td");
}
function addGroupEditPreview() {
  console.debug("BBCode preview: adding group edit preview");
  try {
    const { textarea, target } = getElements({
      textarea: "textarea[name='body']",
      /* Very brittle. Insert after GGn Description prettify if present. */
      target: "textarea[name='body'] ~ br"
    });
    textarea.classList.add("無-bbcode__desc");
    new Preview({
      target,
      where: "beforebegin",
      props: { textarea }
    });
  } catch (err) {
    errorHandler(err, "group edit");
  }
}
function addQuickEditPreview() {
  console.debug("BBCode preview: adding quick edit preview");
  try {
    const { textarea, target, quickEditButton } = getElements({
      textarea: "textarea#group_description_bbcode",
      target: "div.description_div",
      quickEditButton: "a#group_quick_edit"
    });
    textarea.classList.add("無-bbcode__desc");
    const preview = new Preview({
      target,
      props: { textarea }
    });
    preview.toggleHidden(true);
    quickEditButton.addEventListener("click", () => preview.toggleHidden());
  } catch (err) {
    errorHandler(err, "quick edit");
  }
}
function addReviewPreview() {
  console.debug("BBCode preview: adding review preview");
  try {
    const $ = getElements({
      textarea: "textarea#review",
      expandReviewButton: "a#expand_review",
      reviewForm: "form#review_form",
      /** Ehh. */
      expandedReviewBox__container: ".ui-dialog[aria-describedby=expanded_review_box]",
      expandedReviewBox__closeButton: [
        "expandedReviewBox__container",
        "button.ui-dialog-titlebar-close"
      ],
      expandedReviewBox__buttonpane: [
        "expandedReviewBox__container",
        ".ui-dialog-buttonpane"
      ],
      expandedReviewBox__buttonset: [
        "expandedReviewBox__container",
        ".ui-dialog-buttonset"
      ]
    });
    const { controls, preview } = new Preview({
      target: $.reviewForm,
      props: { textarea: $.textarea }
    });
    const classes = ["ui-button", "ui-state-default"];
    $.expandReviewButton.addEventListener("click", () => {
      controls.previewButton.classList.add(...classes);
      $.expandedReviewBox__buttonset.prepend(controls.root);
      $.expandedReviewBox__buttonpane.append(preview.root);
    });
    $.expandedReviewBox__closeButton.addEventListener("click", () => {
      controls.previewButton.classList.remove(...classes);
      $.reviewForm.append(controls.root, preview.root);
    });
  } catch (err) {
    errorHandler(err, "review");
  }
}
function addPostPreview(type = "post") {
  console.debug("BBCode preview: adding post preview");
  try {
    const { textarea, buttons, nativePreviewButton } = getElements({
      textarea: "textarea#quickpost",
      buttons: "div#quickreplybuttons",
      nativePreviewButton: ["buttons", "input[value='Preview']", true]
    });
    textarea.classList.add("無-bbcode__desc");
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea, type }
    });
    if (!nativePreviewButton) {
      buttons.prepend(controls.root);
    } else {
      nativePreviewButton.replaceWith(controls.root);
    }
  } catch (err) {
    errorHandler(err, "post", true);
  }
}
function addNewThreadPreview() {
  console.debug("BBCode preview: adding new thread preview");
  try {
    const { textarea, buttons, nativePreviewButton } = getElements({
      textarea: "textarea#posttext",
      buttons: "div#buttons",
      nativePreviewButton: ["buttons", "input[value='Preview']"]
    });
    textarea.classList.add("無-bbcode__desc");
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea, type: "thread" }
    });
    nativePreviewButton.replaceWith(controls.root);
  } catch (err) {
    errorHandler(err, "thread", true);
  }
}
function addPmPreview() {
  console.debug("BBCode preview: adding pm preview");
  try {
    const { textarea, buttons, nativePreviewButton } = getElements({
      textarea: "textarea[name='body']",
      buttons: "div#buttons",
      nativePreviewButton: [document, "input[value='Preview']", true]
    });
    textarea.classList.add("無-bbcode__desc");
    markParentCell(textarea);
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea }
    });
    if (!nativePreviewButton) {
      buttons.prepend(controls.root);
    } else {
      nativePreviewButton.replaceWith(controls.root);
    }
  } catch (err) {
    errorHandler(err, "pm", true);
  }
}
function fixColspan() {
  document.querySelectorAll("td[colspan='3']").forEach((e) => e.removeAttribute("colSpan"));
}
function addStaffPmPreview() {
  console.debug("BBCode preview: adding staff pm preview");
  fixColspan();
  try {
    const { textarea, buttons, nativePreviewButton } = getElements({
      textarea: "textarea[name='message']",
      buttons: "div#buttons",
      nativePreviewButton: [document, "input#previewbtn", true]
    });
    if (textarea.id !== "quickpost") {
      textarea.classList.add("無-bbcode__desc");
    }
    markParentCell(textarea);
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea }
    });
    if (!nativePreviewButton) {
      buttons.prepend(controls.root);
    } else {
      nativePreviewButton.replaceWith(controls.root);
    }
  } catch (err) {
    errorHandler(err, "staff pm", true);
  }
}
function addWikiPreview() {
  console.debug("BBCode preview: adding wiki preview");
  try {
    const { textarea } = getElements({
      textarea: "textarea[name='body']"
    });
    const target = textarea.nextElementSibling;
    textarea.classList.add("無-bbcode__desc");
    new Preview({
      target,
      props: { textarea }
    });
  } catch (err) {
    errorHandler(err, "wiki");
  }
}
function addGenericPreview(name = "description") {
  console.debug("BBCode preview: adding generic preview");
  const textarea = document.querySelector(`textarea[name='${name}']`);
  if (!textarea) {
    throw new Error(`BBCode preview: ${name} textarea not found`);
  }
  textarea.classList.add("無-bbcode__desc");
  markParentCell(textarea);
  return new Preview({
    target: textarea.parentElement,
    props: { textarea }
  });
}
function addUserEditPreviews() {
  console.debug("BBCode preview: adding user edit preview");
  const $ = getElements(
    {
      info: "textarea[name='info']",
      pgp: "textarea[name='publickey']",
      signature: "textarea[name='signature']"
    },
    false
  );
  for (const [type, textarea] of Object.entries($)) {
    if (!textarea) continue;
    console.debug(`BBCode preview: ${type} textarea found`);
    textarea.classList.add("無-bbcode__desc");
    markParentCell(textarea);
    const previewType = type === "pgp" ? "release" : "group";
    new Preview({
      target: textarea.parentElement,
      props: { textarea, type: previewType }
    });
  }
}
function addUploadPagePreviews() {
  console.debug("BBCode preview: adding upload preview");
  const $ = getElements(
    {
      release: "textarea#release_desc",
      group: "textarea#album_desc"
    },
    false
  );
  const previews = [];
  for (const [type, textarea] of Object.entries($)) {
    if (!textarea) continue;
    console.debug(`BBCode preview: ${type} description textarea found`);
    textarea.classList.add("無-bbcode__desc");
    markParentCell(textarea);
    previews.push(
      new Preview({
        target: textarea.parentElement,
        props: { textarea, type }
      })
    );
  }
  return previews;
}
function uploadPageHandler() {
  try {
    const { categorySelector, dynamicForm } = getElements({
      categorySelector: "select#categories",
      dynamicForm: "div#dynamic_form"
    });
    let previews;
    const observer = new MutationObserver(() => {
      previews = addUploadPagePreviews();
      observer.disconnect();
    });
    categorySelector.addEventListener("change", () => {
      console.debug("BBCode preview: switching category");
      previews.forEach((p) => p.destroy());
      observer.observe(dynamicForm, { childList: true });
    });
    previews = addUploadPagePreviews();
  } catch (err) {
    errorHandler(err, "upload page");
  }
}
function reportV2PageHandler() {
  try {
    const { typeSelector, dynamicForm } = getElements({
      typeSelector: "select#type",
      dynamicForm: "div#dynamic_form"
    });
    let preview;
    const observer = new MutationObserver(() => {
      preview = addGenericPreview("extra");
      observer.disconnect();
    });
    typeSelector.addEventListener("change", () => {
      console.debug("BBCode preview: switching type");
      preview.destroy();
      observer.observe(dynamicForm, { childList: true });
    });
    observer.observe(dynamicForm, { childList: true });
  } catch (err) {
    errorHandler(err, "report page");
  }
}

// src/main.ts
function init() {
  const url = new URL(location.href);
  const pageName = getPageName(url);
  if (pageName === void 0) {
    console.debug("BBCode preview: page not matched");
    return;
  }
  document.body.classList.add(`無__${pageName}`);
  switch (pageName) {
    case PAGE.UPLOAD:
      uploadPageHandler();
      break;
    case PAGE.RELEASE_EDIT:
      addUploadPagePreviews();
      break;
    case PAGE.GROUP_EDIT:
      addGroupEditPreview();
      if (document.querySelector("#prettify_makeitgood_1")) {
        document.body.classList.add("無__has_prettify");
      }
      break;
    case PAGE.GROUP:
      addQuickEditPreview();
      addReviewPreview();
      addPostPreview();
      break;
    case PAGE.ADD_TORRENT_LINK:
    case PAGE.COLLECTION_NEW_OR_EDIT:
    case PAGE.NEW_REQUEST:
      addGenericPreview();
      break;
    case PAGE.VIEW_THREAD:
    case PAGE.VIEW_REQUEST: {
      addPostPreview();
      break;
    }
    case PAGE.COLLECTION: {
      addPostPreview("group");
      break;
    }
    case PAGE.PM: {
      addPmPreview();
      break;
    }
    case PAGE.STAFF_PM: {
      addStaffPmPreview();
      break;
    }
    case PAGE.NEW_THREAD: {
      addNewThreadPreview();
      break;
    }
    case PAGE.REPORT_V1: {
      addGenericPreview("reason");
      break;
    }
    case PAGE.REPORT_V2: {
      reportV2PageHandler();
      break;
    }
    case PAGE.WIKI_CREATE_OR_EDIT: {
      addWikiPreview();
      break;
    }
    case PAGE.USER_EDIT: {
      addUserEditPreviews();
      break;
    }
  }
  GM_addStyle(".無-bbcode__preview-container .box{margin:0}.無-bbcode__preview-container .description_blockquote{margin:auto}.無-bbcode__preview-container{display:block;position:relative;margin:10px 0;clear:both;text-align:start}.無-bbcode__preview-container.hidden{display:none}input[type=button]:disabled{background-color:#767676;box-shadow:none;color:#d5d4d4}.無-bbcode__controls{display:flex;margin:2px 0;flex-flow:row;gap:10px;justify-content:center;align-self:center}.無-bbcode__controls.hidden{display:none}#quickreplybuttons{display:inline-flex!important}#buttons.center{display:flex;justify-content:center}:is(#quickreplybuttons,#buttons){margin:5px 0;gap:5px}:is(#quickreplybuttons,#buttons) input{margin:0!important}#upload #content input[type=url]{width:min(90%,400px)!important}#upload .無-bbcode__desc_td{padding:3px 8px}:is(#collections,#requests,#staffpm) .無-bbcode__desc_td{padding-right:10px}#collections .無-bbcode__controls input{margin:0}#collections .sidebar .無-bbcode__preview-container .box{width:auto}#requests .無-bbcode__desc_td{float:none;display:flex!important;flex-direction:column}#requests .無-bbcode__desc_td br{display:none}#requests #quickpostform{margin-top:10px}#requests #quickreplybuttons input{padding:5px;height:30px}#content textarea.無-bbcode__desc{box-sizing:border-box;width:100%!important;max-width:100%!important;resize:vertical}:is(#torrents,.無__upload_page,.無__new_request_page,.無__staff_pm_page,.無__view_thread_page,.無__report_v1_page,.無__report_v2_page,.無__wiki_create_or_edit_page,.無__user_edit_page) .無-bbcode__desc{margin:0}:is(#quickreplybuttons,.無__pm_page,.無__staff_pm_page,.無__group_edit_page.無__has_prettify,.無__new_thread_page,.無__report_v1_page,.無__wiki_create_or_edit_page) .無-bbcode__controls{display:contents}.無__add_torrent_link_page .無-bbcode__desc_td{padding:0 5px}.無__release_edit_page .無-bbcode__desc_td{padding:3px 6px 3px 3px}.ui-dialog[aria-describedby=expanded_review_box] .ui-dialog-buttonset .無-bbcode__controls{display:inline-block;margin:0}#expanded_review_box textarea#review{box-sizing:border-box;margin:0;width:100%}#newthreadform td{text-align:start;padding:0}#newthreadform td.center{text-align:center}#newthreadform input[type=text]{box-sizing:border-box;margin:2px 0}#newthreadform #title{width:100%!important;margin:5px 0}#newthreadform #answer_block{padding-top:2px}.無__view_thread_page #quickreplybuttons{margin-left:4px}.無__report_v1_page form{box-sizing:border-box}");
}
init();
