import { denoPlugins } from "@luca/esbuild-deno-loader";
import { emptyDir } from "@std/fs";
import esbuild from "esbuild";
import { SCRIPT_NAME } from "./constants.ts";
import { meta } from "./meta.ts";

const style = Deno.readTextFileSync("./src/style.css");

/** Latest tor browser. */
const target = ["firefox128"];

await emptyDir("./dist");

const css = (
  await esbuild.transform(style, {
    loader: "css",
    charset: "utf8",
    target,
    minify: true,
  })
).code.trim();

await esbuild.build({
  entryPoints: ["./src/main.ts"],
  outfile: `./dist/${SCRIPT_NAME}.user.js`,
  platform: "neutral",
  charset: "utf8",
  target,
  bundle: true,
  banner: {
    js: `${meta}\n"use strict";`,
  },
  define: { ___CSS_SLOT___: `'${css}'` },
  dropLabels: ["drop"],
  plugins: [...denoPlugins()],
});

esbuild.stop();
