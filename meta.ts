import {
  AUTHOR,
  SCRIPT_NAME,
  SCRIPT_NAME_HUMAN,
  VERSION,
} from "./constants.ts";

const home = `https://gitlab.com/${AUTHOR}/${SCRIPT_NAME}`;
const support = `${home}/-/issues`;
const download = `${home}/-/raw/master/dist/${SCRIPT_NAME}.user.js`;
const description = "BBCode preview for description fields on GGn.";

export const meta = `// ==UserScript==
// @name        ${SCRIPT_NAME_HUMAN}
// @namespace   green-is-my-paddy
// @version     ${VERSION}
// @description ${description}
// @author      ${AUTHOR}
// @homepageURL ${home}
// @supportURL  ${support}
// @downloadURL ${download}
// @require     https://unpkg.com/@preact/signals-core@1.8.0/dist/signals-core.min.js
// @grant       GM_addStyle
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addValueChangeListener
// @grant       GM_registerMenuCommand
// @grant       GM_unregisterMenuCommand
// @run-at      document-end
// @match       https://gazellegames.net/upload.php
// @match       https://gazellegames.net/torrents.php*
// @match       https://gazellegames.net/collections.php*
// @match       https://gazellegames.net/requests.php*
// @match       https://gazellegames.net/inbox.php*
// @match       https://gazellegames.net/staffpm.php*
// @match       https://gazellegames.net/forums.php*
// @match       https://gazellegames.net/reports.php*
// @match       https://gazellegames.net/reportsv2.php*
// @match       https://gazellegames.net/wiki.php*
// @match       https://gazellegames.net/user.php*
// ==/UserScript==\n`;
