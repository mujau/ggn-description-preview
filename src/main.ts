import type {} from "typed-query-selector";
import { getPageName, PAGE } from "./getPageName.ts";
import {
  addGenericPreview,
  addGroupEditPreview,
  addNewThreadPreview,
  addPmPreview,
  addPostPreview,
  addQuickEditPreview,
  addReviewPreview,
  addStaffPmPreview,
  addUploadPagePreviews,
  addUserEditPreviews,
  addWikiPreview,
  reportV2PageHandler,
  uploadPageHandler,
} from "./previewHandlers.ts";

function init() {
  const url = new URL(location.href);
  const pageName = getPageName(url);
  if (pageName === undefined) {
    console.debug("BBCode preview: page not matched");
    return;
  }

  document.body.classList.add(`無__${pageName}`);
  switch (pageName) {
    case PAGE.UPLOAD:
      uploadPageHandler();
      break;
    case PAGE.RELEASE_EDIT:
      addUploadPagePreviews();
      break;
    case PAGE.GROUP_EDIT:
      addGroupEditPreview();
      if (document.querySelector("#prettify_makeitgood_1")) {
        document.body.classList.add("無__has_prettify");
      }
      break;
    case PAGE.GROUP:
      addQuickEditPreview();
      addReviewPreview();
      addPostPreview();
      break;

    case PAGE.ADD_TORRENT_LINK:
    case PAGE.COLLECTION_NEW_OR_EDIT:
    case PAGE.NEW_REQUEST:
      addGenericPreview();
      break;
    case PAGE.VIEW_THREAD:
    case PAGE.VIEW_REQUEST: {
      addPostPreview();
      break;
    }
    case PAGE.COLLECTION: {
      addPostPreview("group");
      break;
    }
    case PAGE.PM: {
      addPmPreview();
      break;
    }
    case PAGE.STAFF_PM: {
      addStaffPmPreview();
      break;
    }
    case PAGE.NEW_THREAD: {
      addNewThreadPreview();
      break;
    }
    case PAGE.REPORT_V1: {
      addGenericPreview("reason");
      break;
    }
    case PAGE.REPORT_V2: {
      reportV2PageHandler();
      break;
    }
    case PAGE.WIKI_CREATE_OR_EDIT: {
      addWikiPreview();
      break;
    }
    case PAGE.USER_EDIT: {
      addUserEditPreviews();
      break;
    }
  }

  GM_addStyle(___CSS_SLOT___);
}

init();
