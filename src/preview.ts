import { ValueOf } from "type-fest";
import { BBCode } from "./BBCode.ts";
import { errorHandler } from "./common.ts";
import { effect, signal } from "./external.ts";
import { livePreview } from "./livePreviewState.ts";
import { debounce } from "./utils/debounce.ts";
import { getElements } from "./utils/getElements.ts";
import { collectRefs, html } from "./utils/html.ts";
import { propertyObserver } from "./utils/propertyObserver.ts";

export interface Props {
  textarea: HTMLTextAreaElement;
  type?: "group" | "release" | "post" | "thread";
}

export interface Options {
  target?: HTMLElement;
  where?: InsertPosition;
  props: Props;
}

export interface PreviewBinds {
  root: HTMLDivElement;
  inner: HTMLElement;
  other?: {
    threadTitle: HTMLElement;
    form: HTMLFormElement;
    pollQuestion: HTMLElement;
    pollAnswers: HTMLElement;
    threadPoll: HTMLElement;
  } | null;
}

type NewThreadFormElements = HTMLFormControlsCollection & {
  "title": HTMLInputElement;
  "question": HTMLInputElement;
  "answers[]":
    | HTMLInputElement
    | (RadioNodeList & NodeListOf<HTMLInputElement>);
};

function getQuickReplyPreview() {
  try {
    const { root, inner } = getElements(
      {
        root: "#quickreplypreview",
        inner: ["root", "#contentpreview"],
      } as const,
    );
    root.classList.remove("hidden");
    return { root, inner };
  } catch (err) {
    throw errorHandler(err, "quick reply");
  }
}

function getNewThreadPreview() {
  try {
    const { root, inner, ...other } = getElements(
      {
        root: "#newthreadpreview",
        inner: "#contentpreview",
        threadTitle: "#newthreadtitle",
        form: "form#newthreadform",
        pollQuestion: "#poll_question",
        pollAnswers: "#poll_answers",
        threadPoll: "#threadpoll",
      } as const,
    );
    root.classList.remove("hidden");
    return { root, inner, other };
  } catch (err) {
    throw errorHandler(err, "new thread");
  }
}

function getGroupTemplate() {
  const root = html`<div class="box">
    <div class="body" ref="inner"></div>
  </div>` as HTMLDivElement;
  const { inner } = collectRefs(root) as { inner: HTMLQuoteElement };
  return { root, inner };
}

function getReleaseTemplate() {
  const root = html`<table class="torrent_table">
    <tbody>
      <tr>
        <td>
          <blockquote class="description_blockquote" ref="inner"></blockquote>
        </td>
      </tr>
    </tbody>
  </table>` as HTMLDivElement;
  const { inner } = collectRefs(root) as { inner: HTMLQuoteElement };
  return { root, inner };
}

function getControls() {
  const root = html`<div class="無-bbcode__controls">
    <input
      type="button"
      class="無-bbcode__preview-button"
      value="Preview"
      ref="previewButton"
    />
  </div>` as HTMLDivElement;
  const { previewButton } = collectRefs(root) as {
    previewButton: HTMLInputElement;
  };

  return { root, previewButton };
}

const previewBoxMap = {
  group: getGroupTemplate,
  release: getReleaseTemplate,
  post: getQuickReplyPreview,
  thread: getNewThreadPreview,
} as const;

function getPreview(type: NonNullable<Props["type"]>): PreviewBinds {
  const box = previewBoxMap[type]() as ReturnType<
    ValueOf<typeof previewBoxMap>
  >;
  const root = html`<div class="無-bbcode__preview-container hidden">
    ${box.root}
  </div>` as HTMLDivElement;
  const other = "other" in box ? box.other : null;
  return { root, inner: box.inner, other };
}

const DEBOUNCE_DELAY = 2000;

export class Preview {
  controls = getControls();
  preview: PreviewBinds;
  #isHidden = false;
  #isPreviewOpened = false;
  #isPreviewAllowed = signal(false);
  #previewType: NonNullable<Props["type"]>;
  #source = "";
  #markup = "";
  #countdown = signal(Infinity);
  #effects: ReturnType<typeof effect>[];

  constructor({
    target,
    where = "beforeend",
    props: { textarea, type = "group" },
  }: Options) {
    this.preview = getPreview(type);
    this.#previewType = type;
    if (target) {
      target.insertAdjacentElement(where, this.controls.root);
      this.controls.root.after(this.preview.root);
    }
    this.controls.previewButton.addEventListener("click", this.#preview);

    this.#isPreviewAllowed.value = !!textarea.value;
    this.#source = textarea.value;

    this.#effects = [
      /* Preview button text */
      effect(() => {
        this.controls.previewButton.value = livePreview.value
          ? this.#countdown.value === Infinity
            ? "Live preview enabled"
            : `Live preview in ${this.#countdown.value}...`
          : "Preview";
      }),
      /* Preview button disabled state */
      effect(() => {
        this.controls.previewButton.disabled = livePreview.value ||
          !this.#isPreviewAllowed.value;
      }),
    ];

    const [debouncedPreview] = debounce(
      () => {
        this.#preview();
        this.#countdown.value = Infinity;
      },
      DEBOUNCE_DELAY,
      {
        onTick: () => this.#countdown.value--,
        tickInterval: 1001,
        onSetup: () => (this.#countdown.value = DEBOUNCE_DELAY / 1000),
      },
    );

    const textareaValueChange = (val: string) => {
      this.#source = val;
      if (livePreview.value) {
        console.log("BBCode preview: debouncing live preview...");
        debouncedPreview();
      } else {
        this.#isPreviewAllowed.value = !!val;
      }
    };

    textarea.addEventListener("input", () => {
      textareaValueChange(textarea.value);
    });

    propertyObserver(textarea, "value", (val) => {
      textareaValueChange(val as string);
    });

    if (type === "thread") {
      const { threadTitle, form, pollQuestion, pollAnswers } = this.preview
        .other!;
      const titleInput = (form.elements as NewThreadFormElements)["title"];
      const [debouncedPollPreview] = debounce(this.#pollPreview, 500);
      titleInput.addEventListener(
        "input",
        () => (threadTitle.innerText = titleInput.value),
      );
      pollQuestion.addEventListener("input", debouncedPollPreview);
      pollAnswers.addEventListener("input", debouncedPollPreview);
    }
  }

  #togglePreviewOpened(open?: boolean) {
    this.#isPreviewOpened = open ?? !this.#isPreviewOpened;
    this.preview.root.classList.toggle("hidden", !this.#isPreviewOpened);
  }

  #renderPreview() {
    this.preview.inner.innerHTML = this.#markup;
    BBCode(this.preview.inner);
  }

  #pollPreview = () => {
    const { form, threadPoll } = this.preview.other!;
    const elements = form.elements as NewThreadFormElements;
    const answers = Symbol.iterator in elements["answers[]"]
      ? elements["answers[]"]
      : [elements["answers[]"]];
    const elems: HTMLLIElement[] = [];
    let i = 1;
    for (const { value } of answers) {
      const s = i.toString();
      elems.push(
        html`<li>
          <input type="radio" name="vote" id="answer_${s}" value="${s}" />
          <label for="answer_${s}">${value}</label>
          <br />
        </li>` as HTMLLIElement,
      );
      i++;
    }

    threadPoll.replaceChildren(
      html`<p><strong>${elements["question"].value}</strong></p>
        <div id="poll_results">
          <ul style="list-style: none;" id="poll_options">
            ${elems}
            <li>
              <br />
              <input type="radio" name="vote" id="answer_0" value="0" />
              <label for="answer_0">Blank - Show the results!</label>
              <br />
            </li>
          </ul>
          <input type="button" style="float: left;" value="Vote" />
        </div>`,
    );
  };

  #preview = async () => {
    console.debug("BBCode preview: previewing...");
    this.#isPreviewAllowed.value = false;
    if (!this.#source) {
      console.debug("BBCode preview: tried to preview nothing");
      return;
    }
    const formdata = new FormData();
    formdata.append("body", this.#source);
    try {
      const response = await fetch("ajax.php?action=preview", {
        method: "POST",
        headers: {
          "x-requested-with": "XMLHttpRequest",
        },
        body: formdata,
      });
      this.#markup = await response.text();
    } catch (err) {
      if (err instanceof Error) this.#markup = err.message;
      this.#isPreviewAllowed.value = true;
      throw err;
    } finally {
      this.#renderPreview();
      this.#togglePreviewOpened(true);
      if (this.#previewType === "thread") this.#pollPreview();
    }
  };

  toggleHidden = (hide?: boolean): void => {
    this.#isHidden = hide ?? !this.#isHidden;
    this.controls.root.classList.toggle("hidden", this.#isHidden);
    if (this.#isHidden || this.#isPreviewOpened) {
      this.preview.root.classList.toggle("hidden", this.#isHidden);
    }
  };

  destroy = (): void => {
    this.#effects.forEach((dispose) => dispose());
  };
}
