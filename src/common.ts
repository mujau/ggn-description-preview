import { NotFoundError } from "./utils/getElements.ts";

export function errorHandler(
  err: unknown,
  scope: string,
  graceful = false,
): void {
  if (err instanceof NotFoundError) {
    const msg = `BBCode preview: ${scope} element \`${err.cause}\` not found`;
    if (!graceful) {
      throw new Error(msg);
    } else {
      console.debug(msg);
    }
  } else {
    throw err;
  }
}
