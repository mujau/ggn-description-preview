import { signal } from "./external.ts";

const KEY = "LIVE_PREVIEW_STATE";
let id: string | null = null;

export const livePreview = signal(GM_getValue(KEY, false));

function register() {
  if (id) GM_unregisterMenuCommand(id);
  id = GM_registerMenuCommand(
    `${livePreview.value ? "Disable" : "Enable"} live preview`,
    () => {
      livePreview.value = !livePreview.value;
      GM_setValue(KEY, livePreview.value);
      register();
    },
  );
}
register();

GM_addValueChangeListener(KEY, (key, oldVal, val: boolean) => {
  livePreview.value = val;
  register();
});
