import type { ValueOf } from "type-fest";

/* /upload.php */
function isUploadPage({ pathname }: URL) {
  return pathname === "/upload.php";
}

/* /torrents.php?action=edit&id=<number> */
function isReleaseEditPage({ pathname, searchParams }: URL) {
  return pathname === "/torrents.php" && searchParams.has("action", "edit");
}

/* /torrents.php?action=editgroup&groupid=<number> */
function isGroupEditPage({ pathname, searchParams }: URL) {
  return (
    pathname === "/torrents.php" && searchParams.has("action", "editgroup")
  );
}

/* /torrents.php?id=<number> */
function isGroupPage({ pathname, searchParams }: URL) {
  return pathname === "/torrents.php" && searchParams.has("id");
}

/* /torrents.php?action=add_torrent_link&groupid=<number> */
function isAddTorrentLinkPage({ pathname, searchParams }: URL) {
  return (
    pathname === "/torrents.php" &&
    searchParams.has("action", "add_torrent_link")
  );
}

/** /collections.php&id=<number> */
function isCollectionPage({ pathname, searchParams }: URL) {
  return pathname === "/collections.php" && searchParams.has("id");
}

/* /collections.php?action=new
 * /collections.php?action=edit&collageid=<number> */
function isCollectionNewOrEditPage({ pathname, searchParams }: URL) {
  return (
    pathname === "/collections.php" &&
    (searchParams.has("action", "new") || searchParams.has("action", "edit"))
  );
}

/* /requests.php?action=new */
function isNewRequestPage({ pathname, searchParams }: URL) {
  return pathname === "/requests.php" && searchParams.has("action", "new");
}

/* /requests.php?action=view&id=<number> */
function isViewRequestPage({ pathname, searchParams }: URL) {
  return pathname === "/requests.php" && searchParams.has("action", "view");
}

/* /inbox.php */
function isPmPage({ pathname, searchParams }: URL) {
  return (
    pathname === "/inbox.php" &&
    (searchParams.has("action", "compose") ||
      searchParams.has("action", "viewconv"))
  );
}

/* /staffpm.php */
function isStaffPmPage({ pathname }: URL) {
  /* `action=compose` path isn't needed and `[Compose New]` only adds a hash. */
  return pathname === "/staffpm.php";
}

/** /forums.php?action=viewthread */
function isViewThreadPage({ pathname, searchParams }: URL) {
  return pathname === "/forums.php" && searchParams.has("action", "viewthread");
}

/** /forums.php?action=new */
function isNewThreadPage({ pathname, searchParams }: URL) {
  return pathname === "/forums.php" && searchParams.has("action", "new");
}

/** /reports.php?action=report&type=<string>&id=<number> */
function isReportV1Page({ pathname, searchParams }: URL) {
  return pathname === "/reports.php" && searchParams.has("action", "report");
}

/** /reportsv2.php?action=report&id=<number> */
function isReportV2Page({ pathname, searchParams }: URL) {
  return pathname === "/reportsv2.php" && searchParams.has("action", "report");
}

function isWikiCreateOrEditPage({ pathname, searchParams }: URL) {
  return (
    pathname === "/wiki.php" &&
    (searchParams.has("action", "create") || searchParams.has("action", "edit"))
  );
}

/** /user.php?action=edit&userid=<number> */
function isUserEditPage({ pathname, searchParams }: URL) {
  return pathname === "/user.php" && searchParams.has("action", "edit");
}

export const PAGE_LABELS = [
  "UPLOAD",
  "RELEASE_EDIT",
  "GROUP_EDIT",
  "GROUP",
  "ADD_TORRENT_LINK",
  "COLLECTION",
  "COLLECTION_NEW_OR_EDIT",
  "NEW_REQUEST",
  "VIEW_REQUEST",
  "PM",
  "STAFF_PM",
  "VIEW_THREAD",
  "NEW_THREAD",
  "REPORT_V1",
  "REPORT_V2",
  "WIKI_CREATE_OR_EDIT",
  "USER_EDIT",
] as const;

function lowercase_pagify<T extends readonly string[]>(
  names: T,
): {
  [Key in keyof T & `${number}` as T[Key]]: `${Lowercase<T[Key]>}_page`;
} {
  const e: Record<string, string> = Object.create(null);
  for (const name of names) {
    e[name] = `${name.toLowerCase()}_page`;
  }
  // deno-lint-ignore no-explicit-any
  return Object.freeze(e) as any;
}

export const PAGE = lowercase_pagify(PAGE_LABELS);

const matchers = [
  [isUploadPage, PAGE.UPLOAD],
  [isReleaseEditPage, PAGE.RELEASE_EDIT],
  [isGroupEditPage, PAGE.GROUP_EDIT],
  [isGroupPage, PAGE.GROUP],
  [isAddTorrentLinkPage, PAGE.ADD_TORRENT_LINK],
  [isCollectionPage, PAGE.COLLECTION],
  [isCollectionNewOrEditPage, PAGE.COLLECTION_NEW_OR_EDIT],
  [isNewRequestPage, PAGE.NEW_REQUEST],
  [isViewRequestPage, PAGE.VIEW_REQUEST],
  [isPmPage, PAGE.PM],
  [isStaffPmPage, PAGE.STAFF_PM],
  [isViewThreadPage, PAGE.VIEW_THREAD],
  [isNewThreadPage, PAGE.NEW_THREAD],
  [isReportV1Page, PAGE.REPORT_V1],
  [isReportV2Page, PAGE.REPORT_V2],
  [isWikiCreateOrEditPage, PAGE.WIKI_CREATE_OR_EDIT],
  [isUserEditPage, PAGE.USER_EDIT],
] as const;

export function getPageName(url: URL): ValueOf<typeof PAGE> | undefined {
  for (const [matcher, name] of matchers) {
    if (!matcher(url)) continue;
    console.debug(`BBCode preview: ${name} matched`);
    return name;
  }
}
