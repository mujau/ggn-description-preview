/* v1.1.0 2024-04-01 */

export interface Options {
  onTick?: () => void;
  tickInterval?: number;
  onSetup?: () => void;
}

export function debounce<T extends (...args: Parameters<T>) => ReturnType<T>>(
  fn: T,
  delay: number,
  { onTick, tickInterval, onSetup }: Options = {},
): [(this: ThisParameterType<T>, ...args: Parameters<T>) => void, () => void] {
  let timeout: ReturnType<typeof setTimeout>;
  let interval: ReturnType<typeof setInterval>;

  function debounced(this: ThisParameterType<T>, ...args: Parameters<T>) {
    onSetup?.();
    if (timeout) clearTimeout(timeout);
    if (interval) clearInterval(interval);
    timeout = setTimeout(() => {
      fn.apply(this, args);
      if (interval) clearInterval(interval);
    }, delay);
    if (onTick) interval = setInterval(onTick, tickInterval);
  }

  function dispose() {
    if (timeout) clearTimeout(timeout);
    if (interval) clearInterval(interval);
  }

  return [debounced, dispose];
}
