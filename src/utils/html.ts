/* v1.1.2 2024-03-25 */

type MaybeArray<T> = T | T[];

export function html(
  strings: TemplateStringsArray,
  ...subs: MaybeArray<Node | string>[]
): Element | DocumentFragment {
  const template = document.createElement("template");
  const arr = [strings[0]!];
  subs.forEach((val, i) =>
    arr.push(
      `${typeof val === "string" ? val : `<param i=${i}>`}`,
      strings[i + 1]!,
    ),
  );
  template.innerHTML = arr.join("");
  template.content
    .querySelectorAll<Element & { i: string }>("param[i]")
    .forEach((e) => {
      const sub = subs[+e.getAttribute("i")!]!;
      sub instanceof Array ? e.replaceWith(...sub) : e.replaceWith(sub);
    });

  return template.content.children.length > 1
    ? template.content
    : template.content.children[0]!;
}

export function collectRefs(root: ParentNode): Record<string, Element> {
  const refs: Record<string, Element> = {};
  root.querySelectorAll<Element & { ref: string }>("[ref]").forEach((e) => {
    refs[e.getAttribute("ref")!] = e;
    e.removeAttribute("ref");
  });
  return refs;
}
