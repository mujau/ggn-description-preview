/* v1.0.0 2024-02-18 */

export type PropertyObserverCallback = {
  (newValue: unknown, oldValue: unknown): void;
};

/**
 * Plugs into the setter of given property and invokes the callback on every change.
 * @param obj - The object whose property should be observed for changes.
 * @param prop - The name or `Symbol` of the property.
 * @param callback - The function to invoke.
 */
export function propertyObserver(
  obj: object,
  prop: string | symbol,
  callback: PropertyObserverCallback,
): void {
  const prototype = Object.getPrototypeOf(obj);
  const descriptor = Object.getOwnPropertyDescriptor(prototype, prop);
  if (!descriptor) {
    throw new ReferenceError(
      `Input object doesn't have ${String(prop)} property`,
    );
  }
  const set = descriptor.set;
  if (!set) {
    throw new ReferenceError(`${String(prop)} property doesn't have a setter`);
  }
  descriptor.set = (value) => {
    const oldValue = (obj as Record<typeof prop, unknown>)[prop];
    callback(value, oldValue);
    set.call(obj, value);
  };
  Object.defineProperty(obj, prop, descriptor);
}

export default propertyObserver;
