import type * as Signals from "@preact/signals-core";

declare const preactSignalsCore: typeof Signals;

export const { effect, signal } = preactSignalsCore;
