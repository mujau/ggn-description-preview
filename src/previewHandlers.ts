import { Entries } from "type-fest";
import { errorHandler } from "./common.ts";
import { Preview, Props } from "./preview.ts";
import { getElements } from "./utils/getElements.ts";

function markParentCell({ parentElement }: HTMLElement) {
  if (!(parentElement instanceof HTMLTableCellElement)) return;
  parentElement.classList.add("無-bbcode__desc_td");
}

export function addGroupEditPreview(): void {
  console.debug("BBCode preview: adding group edit preview");
  try {
    const { textarea, target } = getElements(
      {
        textarea: "textarea[name='body']",
        /* Very brittle. Insert after GGn Description prettify if present. */
        target: "textarea[name='body'] ~ br",
      } as const,
    );
    textarea.classList.add("無-bbcode__desc");

    new Preview({
      target,
      where: "beforebegin",
      props: { textarea },
    });
  } catch (err) {
    errorHandler(err, "group edit");
  }
}

export function addQuickEditPreview(): void {
  console.debug("BBCode preview: adding quick edit preview");
  try {
    const { textarea, target, quickEditButton } = getElements(
      {
        textarea: "textarea#group_description_bbcode",
        target: "div.description_div",
        quickEditButton: "a#group_quick_edit",
      } as const,
    );
    textarea.classList.add("無-bbcode__desc");

    const preview = new Preview({
      target,
      props: { textarea },
    });
    preview.toggleHidden(true);

    quickEditButton.addEventListener("click", () => preview.toggleHidden());
  } catch (err) {
    errorHandler(err, "quick edit");
  }
}

export function addReviewPreview(): void {
  console.debug("BBCode preview: adding review preview");
  try {
    const $ = getElements(
      {
        textarea: "textarea#review",
        expandReviewButton: "a#expand_review",
        reviewForm: "form#review_form",
        /** Ehh. */
        expandedReviewBox__container:
          ".ui-dialog[aria-describedby=expanded_review_box]",
        expandedReviewBox__closeButton: [
          "expandedReviewBox__container",
          "button.ui-dialog-titlebar-close",
        ],
        expandedReviewBox__buttonpane: [
          "expandedReviewBox__container",
          ".ui-dialog-buttonpane",
        ],
        expandedReviewBox__buttonset: [
          "expandedReviewBox__container",
          ".ui-dialog-buttonset",
        ],
      } as const,
    );

    const { controls, preview } = new Preview({
      target: $.reviewForm,
      props: { textarea: $.textarea },
    });
    const classes = ["ui-button", "ui-state-default"];

    $.expandReviewButton.addEventListener("click", () => {
      controls.previewButton.classList.add(...classes);
      $.expandedReviewBox__buttonset.prepend(controls.root);
      $.expandedReviewBox__buttonpane.append(preview.root);
    });
    $.expandedReviewBox__closeButton.addEventListener("click", () => {
      controls.previewButton.classList.remove(...classes);
      $.reviewForm.append(controls.root, preview.root);
    });
  } catch (err) {
    errorHandler(err, "review");
  }
}

export function addPostPreview(type: Props["type"] = "post"): void {
  console.debug("BBCode preview: adding post preview");
  try {
    const { textarea, buttons, nativePreviewButton } = getElements(
      {
        textarea: "textarea#quickpost",
        buttons: "div#quickreplybuttons",
        nativePreviewButton: ["buttons", "input[value='Preview']", true],
      } as const,
    );
    textarea.classList.add("無-bbcode__desc");
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea, type },
    });
    if (!nativePreviewButton) {
      buttons.prepend(controls.root);
    } else {
      nativePreviewButton.replaceWith(controls.root);
    }
  } catch (err) {
    errorHandler(err, "post", true);
  }
}

export function addNewThreadPreview(): void {
  console.debug("BBCode preview: adding new thread preview");
  try {
    const { textarea, buttons, nativePreviewButton } = getElements(
      {
        textarea: "textarea#posttext",
        buttons: "div#buttons",
        nativePreviewButton: ["buttons", "input[value='Preview']"],
      } as const,
    );
    textarea.classList.add("無-bbcode__desc");
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea, type: "thread" },
    });
    nativePreviewButton.replaceWith(controls.root);
  } catch (err) {
    errorHandler(err, "thread", true);
  }
}

export function addPmPreview(): void {
  console.debug("BBCode preview: adding pm preview");
  try {
    const { textarea, buttons, nativePreviewButton } = getElements(
      {
        textarea: "textarea[name='body']",
        buttons: "div#buttons",
        nativePreviewButton: [document, "input[value='Preview']", true],
      } as const,
    );
    textarea.classList.add("無-bbcode__desc");
    markParentCell(textarea);
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea },
    });
    if (!nativePreviewButton) {
      buttons.prepend(controls.root);
    } else {
      nativePreviewButton.replaceWith(controls.root);
    }
  } catch (err) {
    errorHandler(err, "pm", true);
  }
}

/** Colspan copypasted from inbox that causes really nasty layout issues when
 * you insert more stuff into the table. */
function fixColspan() {
  document
    .querySelectorAll("td[colspan='3']")
    .forEach((e) => e.removeAttribute("colSpan"));
}

export function addStaffPmPreview(): void {
  console.debug("BBCode preview: adding staff pm preview");
  fixColspan();
  try {
    const { textarea, buttons, nativePreviewButton } = getElements(
      {
        textarea: "textarea[name='message']",
        buttons: "div#buttons",
        nativePreviewButton: [document, "input#previewbtn", true],
      } as const,
    );
    if (textarea.id !== "quickpost") {
      textarea.classList.add("無-bbcode__desc");
    }
    markParentCell(textarea);
    const { controls } = new Preview({
      target: buttons,
      where: "afterend",
      props: { textarea },
    });
    if (!nativePreviewButton) {
      buttons.prepend(controls.root);
    } else {
      nativePreviewButton.replaceWith(controls.root);
    }
  } catch (err) {
    errorHandler(err, "staff pm", true);
  }
}

export function addWikiPreview(): void {
  console.debug("BBCode preview: adding wiki preview");
  try {
    const { textarea } = getElements(
      {
        textarea: "textarea[name='body']",
      } as const,
    );
    const target = textarea.nextElementSibling as HTMLElement;
    textarea.classList.add("無-bbcode__desc");
    new Preview({
      target,
      props: { textarea },
    });
  } catch (err) {
    errorHandler(err, "wiki");
  }
}

export function addGenericPreview(name = "description"): Preview {
  console.debug("BBCode preview: adding generic preview");
  const textarea = document.querySelector(`textarea[name='${name}']`);
  if (!textarea) {
    throw new Error(`BBCode preview: ${name} textarea not found`);
  }
  textarea.classList.add("無-bbcode__desc");
  markParentCell(textarea);
  return new Preview({
    target: textarea.parentElement!,
    props: { textarea },
  });
}

export function addUserEditPreviews(): void {
  console.debug("BBCode preview: adding user edit preview");
  const $ = getElements(
    {
      info: "textarea[name='info']",
      pgp: "textarea[name='publickey']",
      signature: "textarea[name='signature']",
    } as const,
    false,
  );
  for (const [type, textarea] of Object.entries($) as Entries<typeof $>) {
    if (!textarea) continue;
    console.debug(`BBCode preview: ${type} textarea found`);
    textarea.classList.add("無-bbcode__desc");
    markParentCell(textarea);
    const previewType = type === "pgp" ? "release" : "group";
    new Preview({
      target: textarea.parentElement!,
      props: { textarea, type: previewType },
    });
  }
}

export function addUploadPagePreviews(): Preview[] {
  console.debug("BBCode preview: adding upload preview");
  const $ = getElements(
    {
      release: "textarea#release_desc",
      group: "textarea#album_desc",
    } as const,
    false,
  );
  const previews: Preview[] = [];
  for (const [type, textarea] of Object.entries($) as Entries<typeof $>) {
    if (!textarea) continue;
    console.debug(`BBCode preview: ${type} description textarea found`);
    textarea.classList.add("無-bbcode__desc");
    markParentCell(textarea);
    previews.push(
      new Preview({
        target: textarea.parentElement!,
        props: { textarea, type },
      }),
    );
  }
  return previews;
}

export function uploadPageHandler(): void {
  try {
    const { categorySelector, dynamicForm } = getElements({
      categorySelector: "select#categories",
      dynamicForm: "div#dynamic_form",
    });
    let previews: Preview[];
    const observer = new MutationObserver(() => {
      previews = addUploadPagePreviews();
      observer.disconnect();
    });
    categorySelector.addEventListener("change", () => {
      console.debug("BBCode preview: switching category");
      previews.forEach((p) => p.destroy());
      observer.observe(dynamicForm, { childList: true });
    });
    previews = addUploadPagePreviews();
  } catch (err) {
    errorHandler(err, "upload page");
  }
}

export function reportV2PageHandler(): void {
  try {
    const { typeSelector, dynamicForm } = getElements({
      typeSelector: "select#type",
      dynamicForm: "div#dynamic_form",
    });
    let preview: Preview;
    const observer = new MutationObserver(() => {
      preview = addGenericPreview("extra");
      observer.disconnect();
    });
    typeSelector.addEventListener("change", () => {
      console.debug("BBCode preview: switching type");
      preview.destroy();
      observer.observe(dynamicForm, { childList: true });
    });
    observer.observe(dynamicForm, { childList: true });
  } catch (err) {
    errorHandler(err, "report page");
  }
}
