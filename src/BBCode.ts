function spoilerShim(elem: HTMLElement) {
  elem.removeAttribute("onclick");
  elem.addEventListener("click", () => {
    const nextSibling = elem.nextElementSibling;
    if (!nextSibling) return;
    elem.innerText = nextSibling.classList.contains("hidden") ? "Hide" : "Show";
    nextSibling.classList.toggle("hidden");
  });
}

/* I don't get why this isn't pure CSS. */
function redactedShim(elem: HTMLElement) {
  elem.addEventListener(
    "mouseenter",
    () =>
      elem.classList.replace("redacted_bbcode_hidden", "redacted_bbcode_show"),
  );
  elem.addEventListener(
    "mouseout",
    () =>
      elem.classList.replace("redacted_bbcode_show", "redacted_bbcode_hidden"),
  );
}

function shim(elem: HTMLElement) {
  elem
    .querySelectorAll<HTMLElement>("[onclick='BBCode.spoiler(this);']")
    .forEach((e) => spoilerShim(e));
  elem
    .querySelectorAll<HTMLElement>(".redacted_bbcode")
    .forEach((e) => redactedShim(e));
  // I can't shim [hidden] because it requires Advanced BBCode perk :/
}

export const BBCode = unsafeWindow.bbcodeOnPageLoad
  ? () => {
    console.debug("BBCode preview: applying BBCode magic...");
    /** Apparently this function should be ran after BBCode preview. Not
     * available on every route. */
    unsafeWindow.bbcodeOnPageLoad!();
  }
  : (root: HTMLElement) => {
    console.debug("BBCode preview: `bbcode.js` unavailable. Shimming...");
    shim(root);
  };
