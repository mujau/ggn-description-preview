# GGn Description Preview

BBCode preview for ~~description~~ various fields on GGn.

## Instalation

The script is tested in the latest stable chromium with [violentmonkey](https://violentmonkey.github.io/).

To install, load `./dist/ggn-description-preview.user.js` [(click)](https://gitlab.com/mujau/ggn-description-preview/-/raw/master/dist/ggn-description-preview.user.js) in your userscript manager.

## Building

The project is managed with ~~pnpm~~ [Deno](https://deno.com/).

- Run `deno task build` to build.
